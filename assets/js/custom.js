(function($, d, w) {
    'use strict';

    $(function() {
        $.toastDefaults.position = 'top-right';
        $.toastDefaults.dismissible = true;
        $.toastDefaults.stackable = true;
        $.toastDefaults.pauseDelayOnHover = true;

        var $formContainer = $('#form-steps-container');

        if ($formContainer.length) {
            var $formSteps = $formContainer.find('.form-step');

            if ($formSteps.length > 0) {
                $formSteps.first().addClass('selected');

                $formSteps.each(function(index) {
                    var $self = $(this);

                    // bloque la propagation de l'évènement click si l'étape n'est pas active
                    $self.on('click', function(e) {
                        if (!$(this).hasClass('selected')) {
                            e.preventDefault();
                            e.stopPropagation();
                        }
                    });

                    // écoute l'évenèment click des boutons de chaque étape
                    $self.find('button.step-trigger-button').click(function(e) {
                        e.preventDefault();
                        e.stopPropagation();

                        // si le bouton cliqué est pour l'étape active
                        if ($self.hasClass('selected')) {
                            // vérifie si le bouton est pour la dernière étape
                            if (index == $formSteps.length - 1) {
                                // lance la dernière action
                                doLastAction();
                            } else {
                                // sinon on est surement dans les étapes qui précèdent la dernière

                                // on déselectionne l'étape en cours
                                $formContainer
                                    .find('.form-step.selected')
                                    .removeClass('selected');

                                // on sélectionne l'étape qui suit
                                var $next = $($formSteps.get(index + 1));
                                $next.addClass('selected');
                                scrollTo($next);
                            }
                        }
                    });
                });
            }
        }

        // c'est dans cette fonction que l'on mettra la dernière action
        function doLastAction() {
            $.snack('info', 'Hello world', 10);
        }

        function scrollTo($el) {
            $('html, body').animate({
                    scrollTop: $el.offset().top,
                },
                'slow'
            );
        }
    });
})(jQuery, document, window);